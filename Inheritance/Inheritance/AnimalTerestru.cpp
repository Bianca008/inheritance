#include "AnimalTerestru.h"

AnimalTerestru::AnimalTerestru(int nrPicioare, int varsta) {
	m_nrPicioare = nrPicioare;
	m_varsta = varsta;
}

void AnimalTerestru::seteazaNume(const std::string& nume) {
	m_nume.assign(nume);
	std::cout << m_nume << "\n";
}

void AnimalTerestru::mananca() {
	std::cout << "Animalul se hraneste.\n";
}

std::ostream& operator<<(std::ostream& out, AnimalTerestru& animal) {
	out << "Numele animalului este: " << animal.m_nume << ".\n";
	out << "Acesta are: " << animal.m_nrPicioare << " picioare.\n";
	out << "Si are varsta de: " << animal.m_varsta << " ani.\n";
	return out;
}