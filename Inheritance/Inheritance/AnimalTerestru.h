#pragma once
#include<iostream>
#include<string>
#include<cstring>

class AnimalTerestru {
protected:
	int m_nrPicioare, m_varsta;
	std::string m_nume;
public:
	AnimalTerestru(int nrPicioare = -1, int varsta = -1);
	~AnimalTerestru() = default;

	void seteazaNume(const std::string& nume = "undefined");
	virtual void mananca();
	friend std::ostream& operator<<(std::ostream&, AnimalTerestru&);
};