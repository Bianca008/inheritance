#include "Caine.h"

Caine::Caine() {
	m_nrPicioare = 4;
	m_culoare_blana = culoare::MARO;
}

void Caine::mananca() {
	std::cout << "Cainele se hraneste cu carne.\n";
}

std::istream& operator>>(std::istream& in, Caine& caine) {
	if (caine.m_nume.empty()){
		std::cout << "Introduceti numele animalului: ";
		getline(in, caine.m_nume);
	}
	std::cout << "Introduceti varsta cainelui: ";
	in >> caine.m_varsta;
	std::cout << "Alegeti una dintre culori pt blana:\n";
	std::cout << "0. Maro\n";
	std::cout << "1. Alb\n";
	std::cout << "2. Negru\n";
	int alegere;
	std::cout << "Alegeti una dintre optiunile de mai sus: ";
	std::cin >> alegere;
	switch (alegere) {
	case 0:
		caine.m_culoare_blana = Caine::culoare::MARO;
		break;
	case 1:
		caine.m_culoare_blana = Caine::culoare::ALB;
		break;
	case 2:
		caine.m_culoare_blana = Caine::culoare::NEGRU;
		break;
	}
	return in;
}

std::ostream& operator<<(std::ostream& out, Caine& caine) {
	out << "Numele animalului este: " << caine.m_nume << ".\n";
	out << "Acesta are: " << caine.m_nrPicioare << " picioare.\n";
	out << "Are varsta de: " << caine.m_varsta << " ani.\n";
	out << "Culoarea blanii este: ";
	switch (static_cast<int>(caine.m_culoare_blana)) {
	case 0:
		std::cout << "Maro.\n";
		break;
	case 1:
		std::cout << "Alb.\n";
		break;
	case 2:
		std::cout << "Negru.\n";
		break;
	}
	return out;
}