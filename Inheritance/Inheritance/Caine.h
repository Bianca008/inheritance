#pragma once
#include "AnimalTerestru.h"

class Caine :public AnimalTerestru {
	enum class culoare {
		MARO,
		ALB,
		NEGRU
	};
	culoare m_culoare_blana;
public:
	Caine();
	~Caine() = default;

	void mananca() override;
	friend std::istream& operator>>(std::istream&, Caine&);
	friend std::ostream& operator<<(std::ostream&, Caine&);
};